/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "dma.h"
#include "iwdg.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "myprint.h"
#include "math.h"
#include "string.h"
#include "stdlib.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

uint8_t  aRxBuff[100];

//char  tempRmBuff[100];
uint8_t count=0;
uint8_t aRxBuffer[1];
uint8_t aTxBuffer[]="ok";
#define LENGTH 8     //接受缓冲区大小
uint8_t	RxBuffer[LENGTH];   //接受缓冲区
uint8_t RxFlag = 0;       //接收完成标志；0表示接受未完成，1表示接收完成
__IO uint8_t uart_dma_recbuf[255] = {0};//串口接收缓冲区
uint16_t i=0;
char exit2=1;
char exit3=1;
char exit4=1;


/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */
    //重装载值=systick 时钟频率(Hz)X想要的定时时间（S）
    //如果时钟频率为：AHB的8分频；AHB=72MHz那么systick的时钟频率为72/8MHz=9MHz
    //若要定时1秒，则重装载值=9000000X1=9000000，调用函数：SysTick_Config(9000000X1);
    //若要定时1毫秒，重状态值=9000000X0.001=90000，调用函数：SysTick_Config(9000000/1000);

    HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/10);// 设置定时时间 重装载值必须小于0XFF FFFF（十进制：16777215）,因为这是一个24位的递减计数器
    HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);//选择SYSTICK分频器，无效？？
    HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);//设定优先级

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART1_UART_Init();
  MX_DMA_Init();
  MX_IWDG_Init();
  /* USER CODE BEGIN 2 */
  RetargetInit(&huart1);

    //串口中断
    __HAL_UART_ENABLE_IT(&huart1,UART_IT_IDLE);//使能串UART IDLE中断 IDLE就是串口收到一帧数据后，发生的中断。什么是一帧数据呢？
    // 比如说给单片机一次发来1个字节，或者一次发来8个字节，这些一次发来的数据，就称为一帧数据，也可以叫做一包数据。
    //RXNE中断和IDLE中断的区别？
    //当接收到1个字节，就会产生RXNE中断，当接收到一帧数据，就会产生IDLE中断。比如给单片机一次性发送了8个字节，就会产生8次RXNE中断，1次IDLE中断。
    //这是状态寄存器，当串口接收到数据时，bit5就会自动变成1，当接收完一帧数据后，bit4就会变成1.
    //需要注意的是，在中断函数里面，需要把对应的位清0，否则会影响下一次数据的接收。比如RXNE接收数据中断，只要把接收到的一个字节读出来，就会清除这个中断。IDLE中断，如何是F0系列的单片机，
    // 需要用ICR寄存器来清除，如果是F1系列的单片机，清除方法是“先读SR寄存器，再读DR寄存器”。（我怎么知道？手册上写的）
    HAL_UART_Receive_DMA(&huart1,(uint8_t *)uart_dma_recbuf, BUFFER_SIZE);//开启USART1的DMA接收
    char str[22];
    utoa(HAL_RCC_GetHCLKFreq(),str,10);
    _write(1,  str, 12); //打印系统时钟频率

    //独立看门狗
    //Tout=((4×2^prer) ×rlr) /32 其中 Tout 为看门狗溢出时间（单位为 ms）；prer 为看门狗时钟预分频值（IWDG_PR 值），
    //范围为 0~7；rlr 为看门狗的重装载值（IWDG_RLR 的值）； 32为当前看门狗独立时钟频率
    //举例：rlr 为 0  那么4乘以2的零次方等于4。hiwdg.Init.Reload的值为：4095 。所以最后的值为 4*4095/32
    //经过实践32kh频率的stm32f407只能设置最大30秒。
    //HAL_IWDG_Init(&hiwdg); //启动独立看门狗

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
    //  HAL_Delay(1000);
    //  HAL_UART_Transmit(&huart1,"s",1,0);
     // _write(1, (char *) Usart_Type.RX_flag, 1);
    //  _write(1,"ssss",4);

      HAL_Delay(1000);  //延时1s
      
//      HAL_GPIO_WritePin(GPIOF,GPIO_PIN_9,GPIO_PIN_RESET);
//      HAL_Delay(1001);
//      HAL_GPIO_WritePin(GPIOF,GPIO_PIN_9,GPIO_PIN_SET);
//      HAL_Delay(1001);
//       char *s="1";
//
//      _write(1,s, strlen(s));
     
  }


  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 168;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

//void HAL_UARTEx_RxEventCallback(UART_HandleTypeDef *huart, uint16_t Size){
//    char *s="ewqrew";
//
//    _write(1,s, 4);
//}
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart){

    if(huart->Instance == USART1)   //判断发生接收中断的串口
    {
        RxFlag=1;   //置为接收完成标志
        //DMA使能接收中断  这个必须添加，否则不能再使用DMA进行发送接受
        //HAL_UART_Receive_DMA(&huart1, (uint8_t *)RxBuffer,LENGTH);
        HAL_UART_Transmit(&huart1,"中断收到数据",12,0);
    }

}

//系统时钟中断
void HAL_SYSTICK_Callback(void){
    if (exit2==0 || exit3 == 0 || exit4== 0){//
        i++;
        if (i>80){
            i=0;
            exit2=1;
            exit3=1;
            exit4=1;
            //  _write(1,"cqwe",4);
        }


    }

   // HAL_UART_Transmit(&huart1,"time中断收到数据",16,0);
}

//GPIO中断
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin){

//    char str1[1];
//    utoa(GPIO_PIN_2,str1,10);
//    char str2[1];
//    utoa(GPIO_PIN_3,str2,10);
//    char str3[1];
//    utoa(GPIO_PIN_4,str3,10);
//    char str4[1];
//    utoa(GPIO_Pin,str4,10);
//    _write(1,str4,1);
//    _write(1,str1,1);
//    _write(1,str2,1);
//    _write(1,str3,1);

    if (GPIO_Pin==GPIO_PIN_3&&exit3){
        exit3=0;
        _write(1,"3",2);
    }
    if (GPIO_Pin==GPIO_PIN_2&&exit2){
        exit2=0;
        _write(1,"2",2);
        HAL_IWDG_Refresh(&hiwdg);
    }
    if (GPIO_Pin==GPIO_PIN_4&&exit4){
        exit4=0;
        _write(1,"4",2);
    }
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

